let zc=
    {
        "type":"FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "properties": {
                    "propietario":"Nombre del agricultor: Adolfo Cuervo Cordoba",
                    "tipoDePlanta":"Tipo de planta: Frijol",
                    "NombreCientifico":"Nombre científico: Phaseolus vulgaris",
                     "FechaPlantacion":"Fecha de plantación: 2020/12/18",
                "Ubicacion":"Ubicación: Bogota, Bochica Central"},
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                        [
                            [
                                -74.10822004079819,
                                4.602626299645764
                            ],
                            [
                                -74.10753607749939,
                                4.603385590119041
                            ],
                            [
                                -74.10826563835144,
                                4.603920301233766
                            ],
                            [
                                -74.10894960165024,
                                4.603099519507663
                            ],
                            [
                                -74.10822004079819,
                                4.602626299645764
                            ]
                        ]
                    ]
                }
            }
        ]
    }

  
