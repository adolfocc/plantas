//crear objeto (variable compuestas) para representar el visor
//L representa a la biblioteca leaflet
let miMapa= L.map('mapid');

//determinar la vista inicial, (ubicacion[bogota],zoom)

miMapa.setView([4.603275974290885,-74.1083487868309],18)

let miProveedor=L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
miProveedor.addTo(miMapa)//GeoJson: Describe objetos geográficos

//Entidad(Features) en Json: Objeto Geográfico + Metadata

//Se recomienda que un GeoJson define Features, no solo la geometría

let mizc= L.geoJson(zc);
mizc.addTo(miMapa)

//crear objeto marcador
let jac= L.marker([4.603275974290885,-74.1083487868309])
jac.bindPopup("<b>Granja Urbana Adolfo</b><br>Planta: Frijol<br>Nombre científico:Phaseolus vulgaris<br>Fecha de Plantación:2020/12/18").openPopup()
jac.addTo(miMapa)

let p1=document.getElementById("p1"),p2=document.getElementById("p2"),p3=document.getElementById("p3"),p4=document.getElementById("p4"),p5=document.getElementById("p5")

p1.textContent=zc.features[0].properties.propietario
p2.textContent=zc.features[0].properties.tipoDePlanta
p3.textContent=zc.features[0].properties.NombreCientifico
p4.textContent=zc.features[0].properties.FechaPlantacion
p5.textContent=zc.features[0].properties.Ubicacion
 